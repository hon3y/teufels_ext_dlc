<?php
namespace TEUFELS\TeufelsExtDlc\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *  			Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *  			Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *  			Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *  			Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *  			Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *  			Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *  			Yannick Aister <y.aister@teufels.com>, teufels GmbH
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class TEUFELS\TeufelsExtDlc\Controller\RenderController.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class RenderControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \TEUFELS\TeufelsExtDlc\Controller\RenderController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('TEUFELS\\TeufelsExtDlc\\Controller\\RenderController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRendersFromRepositoryAndAssignsThemToView()
	{

		$allRenders = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$renderRepository = $this->getMock('TEUFELS\\TeufelsExtDlc\\Domain\\Repository\\RenderRepository', array('findAll'), array(), '', FALSE);
		$renderRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRenders));
		$this->inject($this->subject, 'renderRepository', $renderRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('renders', $allRenders);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenRenderToView()
	{
		$render = new \TEUFELS\TeufelsExtDlc\Domain\Model\Render();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('render', $render);

		$this->subject->showAction($render);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRendersFromRepositoryAndAssignsThemToView()
	{

		$allRenders = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$renderRepository = $this->getMock('TEUFELS\\TeufelsExtDlc\\Domain\\Repository\\RenderRepository', array('findAll'), array(), '', FALSE);
		$renderRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRenders));
		$this->inject($this->subject, 'renderRepository', $renderRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('renders', $allRenders);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRendersFromRepositoryAndAssignsThemToView()
	{

		$allRenders = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$renderRepository = $this->getMock('TEUFELS\\TeufelsExtDlc\\Domain\\Repository\\RenderRepository', array('findAll'), array(), '', FALSE);
		$renderRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRenders));
		$this->inject($this->subject, 'renderRepository', $renderRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('renders', $allRenders);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRendersFromRepositoryAndAssignsThemToView()
	{

		$allRenders = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$renderRepository = $this->getMock('TEUFELS\\TeufelsExtDlc\\Domain\\Repository\\RenderRepository', array('findAll'), array(), '', FALSE);
		$renderRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRenders));
		$this->inject($this->subject, 'renderRepository', $renderRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('renders', $allRenders);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllRendersFromRepositoryAndAssignsThemToView()
	{

		$allRenders = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$renderRepository = $this->getMock('TEUFELS\\TeufelsExtDlc\\Domain\\Repository\\RenderRepository', array('findAll'), array(), '', FALSE);
		$renderRepository->expects($this->once())->method('findAll')->will($this->returnValue($allRenders));
		$this->inject($this->subject, 'renderRepository', $renderRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('renders', $allRenders);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}
}
