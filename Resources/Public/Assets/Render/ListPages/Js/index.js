/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2017
 * Andreas Hafner,
 * Dominik Hilser,
 * Georg Kathan (Author),
 * Hendrik Krüger,
 * Josymar Escalona Rodriguez,
 * Timo Bittner
 * for teufels GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/

var bTxTeufelsExtDlc__debug = false;

var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables = {};

var aTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages = new Array();

/**
 * Init
 */
var oTxTeufelsExtDlc__listpages = function () {

    var init = function () {

        if (0 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            var aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_0 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][0]['id']);
            if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_0 != null) {
                /**
                 * onload
                 */
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][0]['value'] =
                    aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_0.options[aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_0.selectedIndex].value;
                /**
                 * onchange
                 */
                aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_0.onchange = function () {
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][0]['value'] = this.options[this.selectedIndex].value;
                    // setHiddenFieldValues();
                    setSelectFieldValues(0);

                };
            }
        }

        if (1 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            var aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_1 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][1]['id']);
            if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_1 != null) {
                /**
                 * onload
                 */
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][1]['value'] =
                    aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_1.options[aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_1.selectedIndex].value;
                /**
                 * onchange
                 */
                aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_1.onchange = function () {
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][1]['value'] = this.options[this.selectedIndex].value;
                    // setHiddenFieldValues();
                    setSelectFieldValues(1);
                };
            }
        }

        if (2 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            var aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_2 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][2]['id']);
            if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_2 != null) {
                /**
                 * onload
                 */
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][2]['value'] =
                    aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_2.options[aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_2.selectedIndex].value;
                /**
                 * onchange
                 */
                aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_2.onchange = function () {
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][2]['value'] = this.options[this.selectedIndex].value;
                    // setHiddenFieldValues();
                    setSelectFieldValues(2);
                };
            }
        }

        if (3 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            var aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_3 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][3]['id']);
            if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_3 != null) {
                /**
                 * onload
                 */
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][3]['value'] =
                    aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_3.options[aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_3.selectedIndex].value;
                /**
                 * onchange
                 */
                aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectPages_3.onchange = function () {
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][3]['value'] = this.options[this.selectedIndex].value;
                    // setHiddenFieldValues();
                };
            }
        }

        // var sSelectField = 'sys_category_record_mm';
        // if (0 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField]) {
        //     var aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectCategories_0 =
        //         document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][0]['id']);
        //     if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectCategories_0 != null) {
        //         /**
        //          * onload
        //          */
        //         oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][0]['value'] =
        //             aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectCategories_0.options[aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectCategories_0.selectedIndex].value;
        //         /**
        //          * onchange
        //          */
        //         aTxTeufelsextdlcTeufelsextdlcrenderfilter__SelectCategories_0.onchange = function () {
        //             oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][0]['value'] = this.options[this.selectedIndex].value;
        //             // field does not need a hidden value field
        //         };
        //     }
        // }

        return true;

    };

    var setSelectFieldValues = function (iChangedField) {

        if ((iChangedField+1) in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+1)]['value'] = '';
            // setHiddenFieldValues();
            var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+1)]['id']);
            if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1 != null) {
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.setAttribute('disabled', 'disabled');
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.innerHTML = '';
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.innerHTML =
                    '<option value>' + oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.getAttribute('data-default') + '</option>';
            }
        }
        if ((iChangedField+2) in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+2)]['value'] = '';
            // setHiddenFieldValues();
            var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+2)]['id']);
            if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2 != null) {
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2.setAttribute('disabled', 'disabled');
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2.innerHTML = '';
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2.innerHTML =
                    '<option value>' + oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus2.getAttribute('data-default') + '</option>';
            }
        }
        if ((iChangedField+3) in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm']) {
            oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+3)]['value'] = '';
            // setHiddenFieldValues();
            var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3 =
                document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+3)]['id']);

            if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3 != null) {
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3.setAttribute('disabled', 'disabled');
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3.innerHTML = '';
                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3.innerHTML =
                    '<option value>' + oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus3.getAttribute('data-default') + '</option>';
            }
        }

        var sNewValue = oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][iChangedField]['value'];
        var aNewSelectPlus1 = new Array();

        if (typeof tx_teufelsextdlc_teufelsextdlcrenderjspages__pages != 'undefined') {
            switch (iChangedField) {
                case 0:
                    for (var i = 0; i < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages.length; i++) {
                        for (var ii = 0; ii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i].length; ii++) {
                            if (tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['uid'] == sNewValue) {
                                if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]) {
                                    aNewSelectPlus1 = tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'];
                                }
                            }
                        }
                    }

                    break;
                case 1:
                    for (var i = 0; i < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages.length; i++) {
                        for (var ii = 0; ii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i].length; ii++) {
                            if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]) {
                                for (var iii = 0; iii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'].length; iii++) {
                                    if (tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['uid'] == sNewValue) {
                                        if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]) {
                                            aNewSelectPlus1 = tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['subpages'];
                                        }
                                    }
                                }
                            }

                        }
                    }
                    break;
                case 2:
                    for (var i = 0; i < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages.length; i++) {
                        for (var ii = 0; ii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i].length; ii++) {
                            if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]) {
                                for (var iii = 0; iii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'].length; iii++) {
                                    if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]) {
                                        for (var iiii = 0; iiii < tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['subpages'].length; iiii++) {
                                            if (tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['subpages'][iiii]['uid'] == sNewValue) {
                                                if ('subpages' in tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['subpages'][iiii]) {
                                                    aNewSelectPlus1 = tx_teufelsextdlc_teufelsextdlcrenderjspages__pages[i][ii]['subpages'][iii]['subpages'][iiii]['subpages'];
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                        }
                    }
                    break;
            }

            if (aNewSelectPlus1.length > 0) {
                var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1 =
                    document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select']['sys_file_metadata_pages_mm'][(iChangedField+1)]['id']);
                if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1 != null) {
                    var sHtml = '<option value>' + oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.getAttribute('data-default') + '</option>';
                    for (var i = 0; i < aNewSelectPlus1.length; i++) {
                        sHtml += '' +
                            '<option value="' + aNewSelectPlus1[i]["uid"] + '">' +
                            aNewSelectPlus1[i]["title"] +
                            '</option>';
                    }
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.innerHTML = sHtml;
                    oTxTeufelsextdlcTeufelsextdlcrenderlistpages__SelectPages_cplus1.removeAttribute('disabled');
                }
            }

        }

    };

    // var setHiddenFieldValues = function () {
    //     var sSelectField = 'sys_file_metadata_pages_mm';
    //     if (sSelectField in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['hidden']) {
    //         var aValue = new Array();
    //
    //         if (0 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField]) {
    //             if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][0]['value'] != '') {
    //                 aValue.push(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][0]['value']);
    //             }
    //         }
    //
    //         if (1 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField]) {
    //             if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][1]['value'] != '') {
    //                 aValue.push(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][1]['value']);
    //             }
    //         }
    //
    //         if (2 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField]) {
    //             if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][2]['value'] != '') {
    //                 aValue.push(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][2]['value']);
    //             }
    //         }
    //
    //         if (3 in oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField]) {
    //             if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][3]['value'] != '') {
    //                 aValue.push(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['select'][sSelectField][3]['value']);
    //             }
    //         }
    //
    //         var sValue = aValue.length > 0 ? aValue[aValue.length-1] : '';//aValue.join(";") : '';
    //
    //         oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['hidden'][sSelectField]['value'] = sValue;
    //
    //         var aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages =
    //             document.getElementById(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['hidden'][sSelectField]['id']);
    //         if (aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages != null) {
    //             aTxTeufelsextdlcTeufelsextdlcrenderfilter__hiddenPages.value =
    //                 oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['hidden'][sSelectField]['value'];
    //
    //         }
    //
    //     }
    //
    // };

    // var appendToPaginationLinks = function () {
    //
    //     var aPaginationLinks = document.getElementsByClassName(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['pagination']['link']['class']);
    //     var aPaginationAppendSelectHref = document.getElementsByClassName(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['pagination']['append']['select']['class']);
    //
    //     var aPaginationAppendHiddenHref = document.getElementsByClassName(oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables['paramsFilter']['pagination']['append']['hidden']['class']);
    //
    //     for (var i = 0; i < aPaginationLinks.length; i++) {
    //         var href = aPaginationLinks[i].getAttribute('href');
    //         for (var ii = 0; ii < aPaginationAppendSelectHref.length; ii++) {
    //             var sName = aPaginationAppendSelectHref[ii].getAttribute('name');
    //             var sValue = aPaginationAppendSelectHref[ii].options[aPaginationAppendSelectHref[ii].selectedIndex].value;
    //             if(sValue != "") {
    //                 href += "&" + sName + "=" + sValue;
    //             }
    //         }
    //         for (var iii = 0; iii < aPaginationAppendHiddenHref.length; iii++) {
    //             var sName = aPaginationAppendHiddenHref[iii].getAttribute('name');
    //             var sValue = aPaginationAppendHiddenHref[iii].value;
    //             if(sValue != "") {
    //                 href += "&" + sName + "=" + sValue;
    //             }
    //         }
    //         aPaginationLinks[i].setAttribute('href', href);
    //     }
    //
    // };

    if (init()) {
        //setHiddenFieldValues();
        // appendToPaginationLinks();
    }

};

/*
 * Wait till window load.
 *
 */
var iTxTeufelsextdlcTeufelsextdlcrenderlistpages__interval = setInterval(function() {

    if (typeof teufels_cfg_typoscript__windowLoad == "boolean" && teufels_cfg_typoscript__windowLoad) {

        clearInterval(iTxTeufelsextdlcTeufelsextdlcrenderlistpages__interval);

        if (typeof teufels_cfg_typoscript_sStage != 'undefined' &&
            (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development")) {
            console.info('tx_teufels_ext_dlc_render_listpages :: index.js :: loaded');
        }

        /*
         * Check if filter is present in html
         */
        var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__listpages = document.getElementsByClassName('tx-teufels-ext-dlc__listPages');

        if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__listpages.length > 0) {

            if (typeof teufels_cfg_typoscript_sStage != 'undefined' &&
                (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development")) {
                console.info('#tx-teufels-ext-dlc__listpages :: present');
            }

            var oTxTeufelsextdlcTeufelsextdlcrenderlistpages__list = document.getElementsByClassName('tx-teufels-ext-dlc__list');
            if (oTxTeufelsextdlcTeufelsextdlcrenderlistpages__list.length > 0) {

                if (typeof teufels_cfg_typoscript_sStage != 'undefined' &&
                    (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development")) {
                    console.info('#tx-teufels-ext-dlc__list :: present');
                }

                oTxTeufelsextdlcTeufelsextdlcrenderlistpages__variables = {
                    'paramsFilter': {
                        'select': {
                            'sys_file_metadata_pages_mm': {
                                0: {
                                    'id': 'sys_file_metadata_pages_mm__mm__0',
                                    'value': ''
                                },
                                1: {
                                    'id': 'sys_file_metadata_pages_mm__mm__1',
                                    'value': ''
                                },
                                2: {
                                    'id': 'sys_file_metadata_pages_mm__mm__2',
                                    'value': ''
                                },
                                3: {
                                    'id': 'sys_file_metadata_pages_mm__mm__3',
                                    'value': ''
                                }
                            }
                        }
                    }
                };

                /**
                 * Init
                 */
                if (typeof tx_teufelsextdlc_teufelsextdlcrenderjspages__pages != 'undefined') {

                    oTxTeufelsExtDlc__listpages();

                }
            }
        }
    }

}, 1000);