<?php
namespace TEUFELS\TeufelsExtDlc\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2018 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * MonthController
 */
class MonthController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * monthRepository
     *
     * @var \TEUFELS\TeufelsExtDlc\Domain\Repository\MonthRepository
     * @inject
     */
    protected $monthRepository = NULL;
    
    /**
     * action select
     *
     * @return void
     */
    public function selectAction()
    {
        $sPlugin = 'tx_teufelsextdlc_teufelsextdlcrenderlist';
        $aKey = array(
            'tx_teufelsextdlc_domain_model_month',
            'eq'
        );
        $aMonths = $this->monthRepository->findAll()->toArray();
        $aOptions = array();
        foreach ($aMonths as $oMonth) {
            $aOptions[$oMonth->getUid()] = $oMonth->getTitle();
        }
        $aBody = array(
            'aHtml' => array(
                'name' => $sPlugin . '[' . $aKey[0] . '][' . $aKey[1] . ']',
                'id' => 'tx_teufelsextdlc_domain_model_month',
                'aKey' => $aKey,
                'aOptions' => $aOptions,
                'aMonths' => $aMonths
            )
        );
        $tx_teufelsextdlc_teufelsextdlcrenderlist_arguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_teufelsextdlc_teufelsextdlcrenderlist');
        if ($tx_teufelsextdlc_teufelsextdlcrenderlist_arguments != null && $tx_teufelsextdlc_teufelsextdlcrenderlist_arguments != '') {
            if (array_key_exists($aKey[0], $tx_teufelsextdlc_teufelsextdlcrenderlist_arguments)) {
                if (array_key_exists($aKey[1], $tx_teufelsextdlc_teufelsextdlcrenderlist_arguments[$aKey[0]])) {
                    $aMm = intval($tx_teufelsextdlc_teufelsextdlcrenderlist_arguments[$aKey[0]][$aKey[1]]);
                }
                if (array_key_exists($aMm, $aOptions)) {
                    $aBody['aHtml']['sSelected'] = $aMm;
                } else {
                    foreach ($aOptions as $aGroup) {
                        if (is_array($aGroup[1]) && count($aGroup[1]) > 0) {
                            if (array_key_exists($aMm, $aGroup[1])) {
                                $aBody['aHtml']['sSelected'] = $aMm;
                            }
                        }
                    }
                }
            }
        }
        $this->view->assign('bDebug', 0);
        $this->view->assign('aBody', $aBody);
    }

}