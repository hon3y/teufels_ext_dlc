<?php

namespace TEUFELS\TeufelsExtDlc\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

//use TEUFELS\TeufelsExtDlc\Controller\ApiController;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class RoutingController
 * @package TEUFELS\TeufelsExtDlc\Controller
 */
class RoutingController
{

    /**
     * @var \TYPO3\CMS\Extbase\Service\ExtensionService
     */
    protected $extensionService;

    /**
     * @var array
     */
    protected $routes;

    /**
     * @var string
     */
    protected $lastRouteName = null;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
    protected $configurationManager;

    /**
     * Cache of result for getTargetPidByPlugin()
     * @var array
     */
    protected $targetPidPluginCache = [];

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager
     */
    public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * Default contructor.
     */
    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $this->configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        var_dump($this->configurationManager );
        $fullTyposcriptSettings = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\configurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        var_dump($fullTyposcriptSettings );
        $aTxExtDlcTyposcriptSettings = $fullTyposcriptSettings['plugin.']['tx_teufelsextdlc.']['settings.'];
        var_dump($aTxExtDlcTyposcriptSettings);
        $this->extensionService = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Service\\ExtensionService');
    }

    /**
     * Dispatches the request and returns data.
     *
     * @return mixed
     * @throws \RuntimeException
     */
    public function dispatch()
    {

        // make instance ObjectManager
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');

        //$aSettingsDlc = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

        // get ConfigurationManagerInterface
        //$configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');

        // get typoscript configurations from all installed extensions
        $fullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        // extract authors' typoscript settings
        $aTxExtDlcTyposcriptSettings = $fullTyposcriptSettings['plugin.']['tx_teufelsextdlc.']['settings.'];
        var_dump($fullTyposcriptSettings);
        /*
         * aBody
         */


        $aBody = array();
        $aBody['bError'] = 0;

        //$this->initTSFE();

        $controllerParameters = null;
        $response = null;
        $route = GeneralUtility::_GET('route');


//        var_dump($route);
//
//        var_dump($_GET);
        /*
         * Create GuzzleHttp\Client
         */

        $client = new Client(['verify' => false]);
        /*
         * request the API v1700105
         */

        try {
            //$sUrl = 'http://hevrhrbhgbhj.com/';
            $sUrl = (strpos($_SERVER['HOSTNAME'], '.') === false ? 'http://' . $_SERVER['HOSTNAME'] . '/' : '') . 'index.php?id=5&type=666&token=ab03c1995fa06e6d0ec8adf88ecc609d&tx_teufelsextdlc_teufelsextdlcapirequest%5Bmm%5D%5Bsys_file_metadata_pages_mm%5D=2';
            $response = $client->request('GET', $sUrl);
        } catch (RequestException $e) {    $aBody = array(
            'bError' => 1,
            'sDescription' => array(
                'message' => 'Ooops! ' . $e->getMessage(),
                'code' => 'Request Exeption'
            )
        );
        }
        if ($aBody['bError'] == 0) {
            /*
             * Check StatusCode
             */

            $iStatusCode = $response->getStatusCode();
            if ($iStatusCode == 200) {
                /*
                 * Generate cache entry
                 */

                $jBody = $response->getBody();
                $aBody = json_decode($jBody, true);
                //var_dump($aBody);
                $jB = json_encode($aBody);
//                var_dump($jB);
                $sContent = $jB;
            }
        }
//        var_dump($aBody);
//        $this->initTSFE();
//        print('1: ');
//        var_dump($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']);
//        print(':2');
        return json_encode($aBody);
    }

    /**
     * Initializes TSFE and sets $GLOBALS['TSFE'].
     *
     * @return void
     */
    protected function initTSFE()
    {
        $pageId = (GeneralUtility::_GET('id') ? GeneralUtility::_GET('id') : 1);
        /** @var \TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController $tsfe */
//        $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
//            'TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController',
//            $GLOBALS['TYPO3_CONF_VARS'],
//            $pageId,
//            ''
//        );
        $GLOBALS['TSFE']->config['config'] = array();
        $GLOBALS['TSFE']->getConfigArray();
        var_dump($GLOBALS['TSFE']->getConfigArray());

//        $GLOBALS['TSFE'] = GeneralUtility::makeInstance(
//            'TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController',
//            $GLOBALS['TYPO3_CONF_VARS'],
//            $pageId,
//            0,
//            TRUE
//        );

        \TYPO3\CMS\Frontend\Utility\EidUtility::initLanguage();
        \TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();



//        $GLOBALS['TSFE']->initFEuser();
//        // We do not want (nor need) EXT:realurl to be invoked:
//        //$GLOBALS['TSFE']->checkAlternativeIdMethods();
//        $GLOBALS['TSFE']->determineId();
//        $GLOBALS['TSFE']->initTemplate();
//        $GLOBALS['TSFE']->getConfigArray();
//        if ($pageId > 0) {
//            $GLOBALS['TSFE']->settingLanguage();
//        }
//        $GLOBALS['TSFE']->settingLocale();

        // Get linkVars, absRefPrefix, etc
        //\TYPO3\CMS\Frontend\Page\PageGenerator::pagegenInit();
    }

}

/** @var \TEUFELS\TeufelsExtDlc\Controller\RoutingController $routing */
$routing = GeneralUtility::makeInstance('TEUFELS\\TeufelsExtDlc\\Controller\\RoutingController');

try {
    $ret = $routing->dispatch();
} catch (\Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    echo 'Error ' . $e->getCode() . ': ' . $e->getMessage();
    exit;
}

if ($ret === null) {
    header('HTTP/1.0 404 Not Found');
    echo <<<HTML
<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL {$_SERVER['REQUEST_URI']} was not found on this server.</p>
<hr>
<address>Routing Service at {$_SERVER['SERVER_NAME']}</address>
</body></html>
HTML;
    exit();
}

if (empty($ret)) {
    header('HTTP/1.1 204 No Content');
}

header("Content-type: application/json; charset=utf-8");
echo $ret;