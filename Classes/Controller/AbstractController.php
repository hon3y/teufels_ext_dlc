<?php
namespace TEUFELS\TeufelsExtDlc\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * ApiController
 */
/**
 * Class AbstractController
 * @package TEUFELS\TeufelsExtDlc\Controller
 */
class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    //TODO Set Values via constants?
    const B_DEBUG = 0;
    const B_CACHE = 0;

    const PLACE_AT_END_OF_LIST = "Zzzz";

    const VALID_CHARACTERS = '/[^0-9]/';
    const VALID_CHARACTERSFORFIELDS = '/[^0-9A-Za-z]/';

    const CACHE_KEY = 'teufelsextdlc_cache';

    const REQUEST_PLUGIN = 'tx_teufelsextdlc_teufelsextdlcapirequest';
    const REQUEST_CONTROLLER = 'Api';
    const REQUEST_ACTION = 'request';

    const PRE_REQUEST_PLUGIN = 'tx_teufelsextdlc_teufelsextdlcrenderlist';
    const LIST_PLUGIN = 'tx_teufelsextdlc_teufelsextdlcrenderlist';

    const CACHE_IDENTIFIER_PREFIX = 'tx_teufelsextdlc_teufelsextdlcapirequest_';
    const CACHE_IDENTIFIER_PREFIX_RENDER_LIST = 'tx_teufelsextdlc_teufelsextdlcrenderlist_';
    const CACHE_IDENTIFIER_PREFIX_RENDER_LISTONCURRENTPAGE = 'tx_teufelsextdlc_teufelsextdlcrenderlistoncurrentpage_';

    const API_TYPE_NUM = 666;

    /**
     * @return array
     */
    public function getSettingsArray () {
        return $this->settings;
    }

    /**
     * @return int
     */
    protected function getRequestPageUid () {
        $aSettings = $this->getSettingsArray();
        return intval($aSettings['production']['action']['request']);
    }

    /**
     * @return int
     */
    protected function getCurrentPageUid () {
        return intval($GLOBALS['TSFE']->id);
    }

    /**
     * @return int
     */
    protected function getSysLanguageUid () {
        return intval($GLOBALS['TSFE']->sys_language_uid);
    }

    /**
     * @return mixed
     */
    protected function getApiToken () {
        $aSettings = $this->getSettingsArray();
        return $aSettings['production']['api']['token'];
    }

    /**
     * @return mixed
     */
    protected function getApiVersion () {
        $aSettings = $this->getSettingsArray();
        return $aSettings['production']['api']['version'];
    }

    /**
     * @return mixed
     */
    protected function getApiTypeNum () {
        return self::API_TYPE_NUM;
    }

    /**
     * @return mixed
     */
    protected function getApiQueryFilesItemsPerPage () {
        $aSettings = $this->getSettingsArray();
        return $aSettings['production']['api']['query']['files']['itemsPerPage'];
    }

    /**
     * @param $sCacheIdentifier
     * @param $aSettings
     * @return mixed
     */
    protected function getCache ($sCacheIdentifier) {

        $sContent =
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')
                ->getCache(self::CACHE_KEY)
                ->get($sCacheIdentifier);
        return $sContent;

    }

    /**
     * @param $sCacheIdentifier
     * @param $sContent
     * @param $aSettings
     */
    protected function setCache ($sCacheIdentifier, $sContent, $aSettings) {
        if (self::B_CACHE) {
            \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')
                ->getCache(self::CACHE_KEY)
                ->set(
                    $sCacheIdentifier,
                    $sContent,
                    array($sCacheIdentifier),
                    $aSettings['production']['cachePeriod']
                );
        }
    }

    /**
     * @param $aFilterKey
     * @param $aRequestArguments
     * @return array
     */
    protected function sanitizeRequestArguments($aFilterKey, $aRequestArguments, $bDebug = false)
    {
        $aRequestArgumentsSanitized = array();
        switch ($aRequestArguments['version']) {
            case '1':    switch ($aRequestArguments['get']) {
                case 'pages':    /*
                         *  e.g.
                         *  array(1) {
                         *    ["pidList"]=> 1,2
                         *  }
                         */

                    foreach ($aRequestArguments as $sKey => $sValue) {
                        if ($sValue != '') {
                            switch ($sKey) {
                                case 'pidList':
                                    $aRequestArgumentsSanitized[$sKey] = $this->sanitizeString($sValue);
                                    break;
                                default:
                            }
                        }
                    }
                    break;
                case 'files':    /*
                         *  e.g.
                         *  array(1) {
                         *    ["page"]=> 1
                         *  }
                         */

                    foreach ($aRequestArguments as $sKey => $sValue) {
                        if ($sValue != '') {
                            switch ($sKey) {
                                case 'page':
                                    $aRequestArgumentsSanitized[$sKey] = intval($sValue);
                                    break;
                                case 'itemsPerPage':
                                    if (intval($sValue) > 0) {
                                        $aRequestArgumentsSanitized[$sKey] = intval($sValue);
                                    }
                                    break;
                                default:
                            }
                        }
                    }
                    break;
                default:
            }
                break;
            default:
        }
        switch ($aRequestArguments['version']) {
            case '1':    switch ($aRequestArguments['get']) {
                case 'files':
                    /*
                     * e.g.
                     *  array(1) {
                     *    ["move_to_archive_date"]=>
                     *    array(2) {
                     *      ["lt"]=>
                     *      string(4) "1234"
                     *      ["gt"]=>
                     *      string(2) "12"
                     *    }
                     *
                     *  string if one filter is available
                     *
                     *    ["sys_file_metadata_pages"]=>
                     *     array(2) {
                     *      ["mm"]=>
                     *      string(2) "12"
                     *    }
                     *
                     *  array if e.g. multiple page filters are availabe
                     *
                     *    ["sys_file_metadata_pages"]=>
                     *     array(1) {
                     *      ["mm"]=>
                     *      array(2) {
                     *        [0] => "1"
                     *        [1] => "30"
                     *      }
                     *    }
                     *
                     *  }
                     */

                    /*
                     * e.g.
                     *
                     * $sKey: "sys_file_metadata_pages" array()
                     * $aValue: ["mm"] array()
                     */
                    foreach ($aRequestArguments as $sKey => $aValue) {
                        if (is_array($aValue)) {
                            foreach ($aValue as $sKey_ => $mValue_) {
                                /*
                                 * if e.g. "mm", "m1", ...
                                 */

                                if (in_array($sKey_, $aFilterKey)) {
                                    /*
                                     * Should be a string != ''
                                     * or an array eg. if there are multiple page filters
                                     */

                                    switch ($sKey_) {
                                        case 'mm':
                                        case 'm1':
                                            if (is_array($mValue_)) {
                                                $aValue__ = $mValue_;

                                                foreach ($aValue__ as $sKey__ => $aValue___) {
                                                    switch ($sKey__) {
                                                        case 'or':
                                                            if (is_array($aValue___)) {
                                                                $aValue___ = array_filter($aValue___, 'strlen');
                                                                if (count($aValue___) > 0) {
                                                                    $sSanitizedValue = $this->sanitizeString(implode(',', $aValue___), 'mm');
                                                                    $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
                                                                }
                                                            }
                                                            break;
                                                        case 'last':
                                                            if (is_array($aValue___)) {
                                                                $aValue___ = array_filter($aValue___, 'strlen');
                                                                if (count($aValue___) > 0) {
                                                                    if ($aValue___[count($aValue___) - 1] != '') {
                                                                        $sSanitizedValue = $this->sanitizeString($aValue___[count($aValue___) - 1], 'mm');
                                                                        $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
                                                                    }
                                                                }
                                                            }
                                                            break;
                                                        default:

                                                    }
                                                }
                                            } else {
                                                /*
                                                 * string
                                                 */
                                                $sValue_ = $mValue_;
                                                if ($sValue_ != '') {
                                                    switch ($sKey_) {
                                                        case 'mm':
                                                            $sSanitizedValue = $this->sanitizeString($sValue_, 'mm');
                                                            $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
                                                            break;
                                                        case 'm1':
                                                            $sSanitizedValue = $this->sanitizeString($sValue_, 'm1');
                                                            $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
                                                            break;
                                                        default:
                                                    }
                                                }
                                            }
                                            break;
                                        case 'lt':
                                        case 'lte':
                                        case 'eq':
                                        case 'neq':
                                        case 'gte':
                                        case 'gt':
                                            /*
                                             * string
                                             */
                                            $sValue_ = $mValue_;
                                            if ($sValue_ != '') {
                                                switch ($sKey_) {
                                                    case 'lt':
                                                    case 'lte':
                                                    case 'eq':
                                                    case 'neq':
                                                    case 'gte':
                                                    case 'gt':
                                                        $sSanitizedValue = $this->sanitizeString($sValue_);
                                                        $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
                                                        break;
                                                    default:
                                                }
                                            }
                                            break;
                                        default:
                                    }
                                }
                            }
                        }
                    }

//                    foreach ($aFilterKey as $sFilterKeyValue) {
//                        foreach ($aRequestArguments as $sKey => $aValue) {
//                            if (is_array($aValue)) {
//                                foreach ($aValue as $sKey_ => $mValue_) {
//
//                                    /*
//                                     * Should be a string != ''
//                                     * or an array eg. if there are multiple page filters
//                                     */
//
//                                    if (is_array($mValue_)) {
////
//                                    } else {
//                                        /*
//                                         * string
//                                         */
//                                        $sValue_ = $mValue_;
//                                        if ($sValue_ != '') {
//                                            switch ($sKey_) {
//                                                // because filter[field][eq|gt|...]=xxx
//                                                case $sFilterKeyValue:
//                                                    switch ($sFilterKeyValue) {
//                                                        case 'mm':
//                                                            $sSanitizedValue = $this->sanitizeString($sValue_, 'mm');
//                                                            $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
//                                                            break;
//                                                        case 'm1':
//                                                            $sSanitizedValue = $this->sanitizeString($sValue_, 'm1');
//                                                            $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
//                                                            break;
//                                                        case 'lt':
//                                                        case 'lte':
//                                                        case 'eq':
//                                                        case 'neq':
//                                                        case 'gte':
//                                                        case 'gt':
//                                                            $sSanitizedValue = $this->sanitizeString($sValue_);
//                                                            $aRequestArgumentsSanitized[$sKey][$sKey_] = $sSanitizedValue;
//                                                            break;
//                                                        default:
//                                                    }
//                                                    break;
//                                                default:
//                                            }
//                                        }
//                                    }
//
//                                }
//                            }
//                        }
//                    }
                    break;
                default:
            }
            default:
        }
        return $aRequestArgumentsSanitized;
    }

    /**
     * @param $sString
     * @param $sType
     * @return string
     */
    private function sanitizeString($sString, $sType = '')
    {
        switch ($sType) {
            case 'mm':
            case 'm1':
                /*
                 * Only OR via ',' possible
                 *
                 * Be careful: Using ';' will lead to unwanted results!!
                 */
                $aSanitizedOr = array();
                $aNotSanitizedOr= explode(',', $sString);
                foreach ($aNotSanitizedOr as $sNotSanitizedOr_) {
                    if (preg_replace(self::VALID_CHARACTERS, '', $sNotSanitizedOr_) != '') {
                        $aSanitizedOr[] = preg_replace(self::VALID_CHARACTERS, '', $sNotSanitizedOr_);
                    }
                }
                $sSanitizedString = implode(',', $aSanitizedOr);
                return $sSanitizedString;
                break;
//                $aNotSanitizedAnd = explode(';', $sString);
//                $aSanitizedAnd = array();
//                foreach ($aNotSanitizedAnd as $sNotSanitizedOr) {
//                    $aSanitizedOr = array();
//                    $aNotSanitizedOr = explode(',', $sNotSanitizedOr);
//                    foreach ($aNotSanitizedOr as $sNotSanitizedOr_) {
//                        if (preg_replace(self::VALID_CHARACTERS, '', $sNotSanitizedOr_) != '') {
//                            $aSanitizedOr[] = preg_replace(self::VALID_CHARACTERS, '', $sNotSanitizedOr_);
//                        }
//                    }
//                    $aSanitizedAnd[] = implode(',', $aSanitizedOr);
//                }
//                $sSanitizedString = implode(';', $aSanitizedAnd);
//                return $sSanitizedString;
//                break;
            default:    $aNotSanitizedAnd = explode(';', $sString);
                $aSanitizedAnd = array();
                foreach ($aNotSanitizedAnd as $sNotSanitizedOr) {
                    $aSanitizedOr = array();
                    $aNotSanitizedOr = explode(',', $sNotSanitizedOr);
                    foreach ($aNotSanitizedOr as $sNotSanitizedOr_) {
                        if (preg_replace(self::VALID_CHARACTERSFORFIELDS, '', $sNotSanitizedOr_) != '') {
                            $aSanitizedOr[] = preg_replace(self::VALID_CHARACTERSFORFIELDS, '', $sNotSanitizedOr_);
                        }
                    }
                    $aSanitizedAnd[] = implode(',', $aSanitizedOr);
                }
                $sSanitizedString = implode(';', $aSanitizedAnd);
                return $sSanitizedString;
        }
    }

}