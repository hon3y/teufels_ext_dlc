<?php

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$aTypoScriptConfigurationExtDlc = \TEUFELS\TeufelsExtDlc\Utility\ExtConfigurationUtility::getSettings();
if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Core\Utility\DebugUtility::debug($aTypoScriptConfigurationExtDlc, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
}
//var_dump($configurationExtDlc);

$tca_ = [
    'ctrl' => [
        'type' => 'file:type',
    ],
//    'types' => [
//        TYPO3\CMS\Core\Resource\File::FILETYPE_UNKNOWN => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;25,
//
//                --div--;Download Center,
//				    --palette--;;70,
//
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.geo_location;40
//			',
//        ],
//        TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;25,
//
//                --div--;Rel,
//				    show_on_pages,
//
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright, language,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.geo_location;40
//			',
//        ],
//        TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;20,
//
//				    --div--;Rel,
//				    show_on_pages,
//
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright, language,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.geo_location;40,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.gps;30,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.content_date;60,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.camera,
//					color_space,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.metrics;50
//			',
//        ],
//        TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;25,
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright, language,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.content_date;60,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.audio,
//				    duration
//			',
//        ],
//        TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;25,
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright, language,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.content_date;60,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.video,
//					duration
//			',
//        ],
//        TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
//            'showitem' => '
//				fileinfo, title, description, ranking, keywords,
//				    --palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.accessibility;25,
//				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.visibility;10,
//					fe_groups,
//				--div--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:tabs.metadata,
//					creator, creator_tool, publisher, source, copyright, language,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.geo_location;40,
//					pages,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.metrics;50,
//					--palette--;LLL:EXT:filemetadata/Resources/Private/Language/locallang_tca.xlf:palette.content_date;60
//			',
//        ],
//    ],
    'palettes' => [
//        '10' => [
//            'showitem' => 'visible, status',
//        ],
//        '20' => [
//            'showitem' => 'alternative, --linebreak--, caption, --linebreak--, download_name',
//        ],
//        '25' => [
//            'showitem' => 'caption, --linebreak--, download_name',
//        ],
//        '30' => [
//            'showitem' => 'latitude, longitude',
//        ],
//        '40' => [
//            'showitem' => 'location_country, location_region, location_city',
//        ],
//        '50' => [
//            'showitem' => 'width, height, unit',
//        ],
//        '60' => [
//            'showitem' => 'content_creation_date, content_modification_date',
//        ],
//        '70' => [
//            'showitem' => 'show_on_pages',
//        ],
        '332' => [
            'showitem' => 'link_to,--linebreak--,
            show_on_page,--linebreak--,
            show_in_language,--linebreak--,show_in_default_language',
        ],
        '333' => [
            'showitem' => 'sys_category, sortkey, --linebreak--, tx_teufelsextdlc_domain_model_year, tx_teufelsextdlc_domain_model_month',
        ],
        '334' => [
            'showitem' => 'move_to_archive_date, tx_teufelsextdlc_domain_model_language, --linebreak--,
             show_in_tx_teufelsextdlc_domain_model_language',
        ],

    ],
    'columns' => [

        'show_on_page' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.show_on_page',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'foreign_table' => 'pages',
                'foreign_table_where' => 'AND doktype = 1',
                'MM' => 'sys_file_metadata_pages_mm',
                'wizards' => array(
                    'suggest' => array(
                        'type' => 'suggest',
                        'default' => array(
                            'searchWholePhrase' => 1
                        ),
                        'pages' => array(
                            'searchCondition' => 'doktype = 1'
                        ),
                    ),
                ),
            ),
        ),

        'show_in_tx_teufelsextdlc_domain_model_language' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:show_in_tx_teufelsextdlc_domain_model_language',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_teufelsextdlc_domain_model_language',
                'MM' => 'sys_file_metadata_tx_teufelsextdlc_domain_model_language_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'enableMultiSelectFilterTextfield' => TRUE,
                'wizards' => array(
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                ),
            ),
        ),

        'show_in_language' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.show_in_language',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'sys_language',
                'MM' => 'sys_file_metadata_sys_language_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'enableMultiSelectFilterTextfield' => TRUE,
                'wizards' => array(
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                ),
            ),
        ),

        'show_in_default_language' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.show_in_default_language',
            'config' => array(
                'type' => 'check',
                'default' => 1
            )
        ),

        'tx_teufelsextdlc_domain_model_language' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.written_in_language',
            'config' => array(
                'items' => array(
                    array('---', 0)
                ),
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_teufelsextdlc_domain_model_language',
                'foreign_table_where' => 'AND tx_teufelsextdlc_domain_model_language.hidden=0 AND tx_teufelsextdlc_domain_model_language.deleted=0 AND tx_teufelsextdlc_domain_model_language.sys_language_uid IN (-1,0)',
                'minitems' => 0,
                'maxitems' => 1,
            ),
        ),

        'sys_category' => array(
            'exclude' => 0,
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.sys_category',
            'config' => array(
                'items' => array(
                    array('---', 0)
                ),
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => 'AND sys_category.hidden=0 AND sys_category.deleted=0 AND sys_category.parent = ' . $aTypoScriptConfigurationExtDlc['production.']['api.']['query.']['delimiter.']['sys_category'] . ' AND sys_category.sys_language_uid IN (-1,0)',
                'minitems' => 0,
                'maxitems' => 1,
            ),
        ),

        'link_to' => [
            'exclude' => 0,
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.link_to',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'wizards' => [
                    '_PADDING' => 2,
                    'link' => [
                        'type' => 'popup',
                        'title' => 'Link',
                        'icon' => 'link_popup.gif',
                        'module' => [
                            'name' => 'wizard_element_browser',
                            'urlParameters' => [
                                'mode' => 'wizard'
                            ]
                        ],
                        'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
                    ],
                ],
            ],
        ],

        'sortkey' => [
            'exclude' => 0,
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.sortkey',
            'config' => [
                'type' => 'input',
                'size' => 20,
                'eval' => 'trim',
                'default' => 999
            ],
        ],


//        'tx_sdg16extextdlc_domain_model_targetaudience' => array(
//            'exclude' => 0,
//            'label' => 'LLL:EXT:sdg_16_ext_dlc/Resources/Private/Language/translation_db.xlf:tx_sdg16extdlc_domain_model_targetaudience.fieldtitle',
//            'config' => array(
//                'type' => 'select',
//                'renderType' => 'selectMultipleSideBySide',
//                'foreign_table' => 'tx_sdg16extdlc_domain_model_targetaudience',
//                'foreign_table_where' => 'AND tx_sdg16extdlc_domain_model_targetaudience.hidden=0 AND tx_sdg16extdlc_domain_model_targetaudience.deleted=0 AND tx_sdg16extdlc_domain_model_targetaudience.sys_language_uid IN (-1,0)',
//                'MM' => 'sys_file_metadata_tx_sdg16extdlc_targetaudience_mm',
//                'size' => 10,
//                'autoSizeMax' => 30,
//                'maxitems' => 9999,
//                'multiple' => 0,
//                'enableMultiSelectFilterTextfield' => TRUE,
//                'wizards' => array(
//                    '_PADDING' => 1,
//                    '_VERTICAL' => 1,
//                ),
//            ),
//        ),


//        'tx_teufelsextdlc_domain_model_year' => array(
//            'exclude' => 0,
//            'label' => 'Year',
//            'config' => array(
//                'type' => 'select',
//                'renderType' => 'selectSingle',
//                'foreign_table' => 'tx_teufelsextdlc_domain_model_year',
//                'foreign_table_where' => 'AND tx_teufelsextdlc_domain_model_year.hidden=0 AND tx_teufelsextdlc_domain_model_year.deleted=0 AND tx_teufelsextdlc_domain_model_year.sys_language_uid IN (-1,0)',
//                'MM' => 'sys_file_metadata_tx_teufelsextdlc_year_mm',
//                'minitems' => 0,
//                'maxitems' => 1
//            ),
//        ),

        // todo: translate labels
        'tx_teufelsextdlc_domain_model_year' => array(
            'exclude' => 0,
            'label' => 'Year',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_teufelsextdlc_domain_model_year',
                'foreign_table_where' => 'AND tx_teufelsextdlc_domain_model_year.hidden=0 AND tx_teufelsextdlc_domain_model_year.deleted=0 AND tx_teufelsextdlc_domain_model_year.sys_language_uid IN (-1,0)',
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 17
            ),
        ),

        'tx_teufelsextdlc_domain_model_month' => array(
            'exclude' => 0,
            'label' => 'Month',
            'config' => array(
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_teufelsextdlc_domain_model_month',
                'foreign_table_where' => 'AND tx_teufelsextdlc_domain_model_month.hidden=0 AND tx_teufelsextdlc_domain_model_month.deleted=0 AND tx_teufelsextdlc_domain_model_month.sys_language_uid IN (-1,0)',
                'minitems' => 0,
                'maxitems' => 1,
                'default' => 1
            ),
        ),

//
//        'sortyear' => [
//            'exclude' => 0,
//            'l10n_mode' => 'exclude',
//            'l10n_display' => 'defaultAsReadonly',
//            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.sortyear',
//            'config' => [
//                'type' => 'select',
//                'renderType' => 'selectSingle',
//                'items' => array (
//                    array('2025', '2025'),
//                    array('2024', '2024'),
//                    array('2023', '2023'),
//                    array('2022', '2022'),
//                    array('2021', '2021'),
//                    array('2020', '2020'),
//                    array('2019', '2019'),
//                    array('2018', '2018'),
//                    array('2017', '2017'),
//                    array('2016', '2016'),
//                    array('2015', '2015'),
//                    array('2014', '2014'),
//                    array('2013', '2013'),
//                    array('2012', '2012'),
//                    array('2011', '2011'),
//                    array('2010', '2010'),
//                    array('2009', '2009'),
//                    array('2008', '2008'),
//                    array('2007', '2007'),
//                    array('2006', '2006'),
//                    array('2005', '2005'),
//                    array('2004', '2004'),
//                    array('2003', '2003'),
//                    array('2002', '2002'),
//                    array('2001', '2001'),
//                    array('2000', '2000'),
//                ),
//                'minitems' => 0,
//                'maxitems' => 1,
//                'default' => '2018'
//            ],
//        ],
//
//        'sortmonth' => [
//            'exclude' => 0,
//            'l10n_mode' => 'exclude',
//            'l10n_display' => 'defaultAsReadonly',
//            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.sortmonth',
//            'config' => [
//                'type' => 'select',
//                'renderType' => 'selectSingle',
//                'items' => array (
//                    array('1','1'),
//                    array('2','2'),
//                    array('3','3'),
//                    array('4','4'),
//                    array('5','5'),
//                    array('6','6'),
//                    array('7','7'),
//                    array('8','8'),
//                    array('9','9'),
//                    array('10','10'),
//                    array('11','11'),
//                    array('12','12'),
//                ),
//                'minitems' => 0,
//                'maxitems' => 1,
//                'default' => '1'
//            ],
//        ],

        'move_to_archive_date' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'l10n_display' => 'defaultAsReadonly',
            'label' => 'LLL:EXT:teufels_ext_dlc/Resources/Private/Language/translation_db.xlf:sysfilemetadata.move_to_archive_date',
            'config' => [
                'type' => 'input',
                'size' => 12,
                'max' => 20,
                'eval' => 'date',
                'default' => time() + (1000 * 365 * 24 * 60 * 60)
            ],
        ],

    ],
];

$GLOBALS['TCA']['sys_file_metadata'] = array_replace_recursive($GLOBALS['TCA']['sys_file_metadata'], $tca_);

$sPalette = ',--div--;Download Center,
    --palette--;Relations;332,
    --palette--;Sorting;333,
    --palette--;Others;334
';

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'sys_file_metadata',
    $sPalette
);

// Add category tab if categories column is present
if (isset($GLOBALS['TCA']['sys_file_metadata']['columns']['categories'])) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
        'sys_file_metadata',
        '--div--;LLL:EXT:lang/locallang_tca.xlf:sys_category.tabs.category,categories'
    );
}
