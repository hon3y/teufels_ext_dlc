<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcapirequest',
	array(
		'Api' => 'request',
		
	),
	// non-cacheable actions
	array(
		'Api' => 'request',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistoncurrentpage',
	array(
		'Render' => 'listOnCurrentPage',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'listOnCurrentPage',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlist',
	array(
		'Render' => 'list',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'list',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderfilter',
	array(
		'Render' => 'filter',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'filter',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistcategories',
	array(
		'Render' => 'listCategories',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'listCategories',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistlanguages',
	array(
		'Render' => 'listLanguages',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'listLanguages',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistpages',
	array(
		'Render' => 'listPages',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'listPages',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderjspages',
	array(
		'Render' => 'jsPages',
		
	),
	// non-cacheable actions
	array(
		'Render' => 'jsPages',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcselectyear',
	array(
		'Year' => 'select',
		
	),
	// non-cacheable actions
	array(
		'Year' => 'select',
		
	)
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcselectmonth',
	array(
		'Month' => 'select',
		
	),
	// non-cacheable actions
	array(
		'Month' => 'select',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include'][$_EXTKEY] = 'EXT:' . $_EXTKEY . '/Classes/Controller/RoutingController.php';

/**
 * register cache for extension
 */
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['teufelsextdlc_cache'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['teufelsextdlc_cache'] = array();
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['teufelsextdlc_cache']['frontend'] = 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['teufelsextdlc_cache']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['teufelsextdlc_cache']['options']['compression'] = 1;
}