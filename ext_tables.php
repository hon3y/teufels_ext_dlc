<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcapirequest',
	'Download Center :: API :: Request'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistoncurrentpage',
	'Download Center :: Render :: List on current page'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlist',
	'Download Center :: Render :: List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderfilter',
	'Download Center :: Render :: Filter'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistcategories',
	'Download Center :: Render :: Category List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistlanguages',
	'Download Center :: Render :: Language List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderlistpages',
	'Download Center :: Render :: Page List'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcrenderjspages',
	'Download Center :: Render :: Page JS'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcselectyear',
	'Download Center :: Select :: Year'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'TEUFELS.' . $_EXTKEY,
	'Teufelsextdlcselectmonth',
	'Download Center :: Select :: Month'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'teufels_ext_dlc');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextdlc_domain_model_api', 'EXT:teufels_ext_dlc/Resources/Private/Language/locallang_csh_tx_teufelsextdlc_domain_model_api.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextdlc_domain_model_api');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextdlc_domain_model_render', 'EXT:teufels_ext_dlc/Resources/Private/Language/locallang_csh_tx_teufelsextdlc_domain_model_render.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextdlc_domain_model_render');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextdlc_domain_model_language', 'EXT:teufels_ext_dlc/Resources/Private/Language/locallang_csh_tx_teufelsextdlc_domain_model_language.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextdlc_domain_model_language');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextdlc_domain_model_year', 'EXT:teufels_ext_dlc/Resources/Private/Language/locallang_csh_tx_teufelsextdlc_domain_model_year.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextdlc_domain_model_year');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextdlc_domain_model_month', 'EXT:teufels_ext_dlc/Resources/Private/Language/locallang_csh_tx_teufelsextdlc_domain_model_month.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextdlc_domain_model_month');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/***************
 * Add page TSConfig
 * for BackendLayout(s)
 */
$pageTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TsConfig/Page/config.txt');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);